# Vocabulário pt_BR - Termos mais comuns utilizados em traduções


## Equipe brasileira de tradução

Contatos:

 * Lista de discussão: debian-l10n-portuguese@lists.debian.org
 * Wiki: https://wiki.debian.org/Brasil/Traduzir

### Apresentação

Esta é uma tabela colaborativa originalmente baseada na wordlist do DDTP/DDTSS.
Esse vocabulário tem dois principais objetivos:

 1. Diminuir as dúvidas dos(as) tradutores(as) em relação à tradução de
alguns termos complexos, anglicismos ou termos sem uso difundido em português.

 2. Manter um padrão na tradução dos termos.


### Recomendações para construção e modificação do vocabulário

Esta é uma tabela colaborativa, qualquer pessoa que possua login no sistema
Salsa pode contribuir. Recomendamos fortemente que todos(as) os(as)
participantes da equipe de tradução façam seu cadastro no Salsa.

Estamos abertos(as) a sugestões de mudança, seja no formato do arquivo, seja
na melhoria dos processos. Basta enviar um e-mail para a lista apresentando
sua proposta e debateremos

Caso tenha dúvidas em relação a algum termo e sua tradução, envie um e-mail
para a lista e peça a opinião da equipe para que possamos coletivamente definir
qual a melhor opção.


### Padronização do arquivo de vocabulário

Pedimos a gentileza de manter a padronização:

 * **Arquivo com os termos**: vocabulario-pt_BR.md
 * **Formato do arquivo**   : Markdown, UTF-8 Unicode text
 * **Ordenação dos termos** : ordem alfabética
 * **Tamanho da entrada**   : quantidade de texto indiferente, sendo objetivo e conciso
 * **Formato das entradas** : Original em inglês | Tradução | Exemplo de uso/comentário

Cada entrada deve ser composta por um único termo, mesmo que ocorram
sinônimos, para que seja possível fazer buscas exatas. Por exemplo, a entrada
abaixo está incorreta:

_built in, built-in | embutido |_

O procedimento correto é inserir os vocábulos como entradas diferentes:

_built in | embutido |_  
_built-in | embutido |_

O arquivo texto final é ordenado via comando 'sort':

`sort --ignore-case --output=vocabulario-pt_BR.md`

### Script de validação de formato

Um script é fornecido para verificação da formatação do arquivo de vocabulário.
Isto evita que problemas sejam propagados para outros sistemas durante a
conversão. Sugere-se executar este script **antes** de qualquer conversão.

Execute o script `./valida-formatacao-do-vocabulario.sh` no mesmo diretório
onde está o arquivo do vocabulário (vocabulario-pt_BR.md)

### Modo de utilização em geral

O vocabulário pode ser usado em qualquer frente da equipe de tradução: nas
páginas web, wiki, pacotes e descrição de pacotes, etc.

O arquivo pode ser automaticamente baixado e atualizado pelo sistema de
versionamento git/Salsa, mantendo-se uma cópia no computador local. Pode-se
assim consultá-lo facilmente por qualquer meio que o(a) tradutor(a) achar
conveniente.


### Modo de utilização para o sistema DDTP/DDTSS

Atualmente este arquivo de vocabulário é a fonte para o vocabulário do sistema
DDTP/DDTSS. Basta renomeá-lo e exportá-lo para o formato específico daquele
sistema.

As especificações do vocabulário DDTP/DDTSS:

 * **Repositório DDTP**    : https://salsa.debian.org/l10n-team/ddtp
 * **Arquivo**             : ddtp/ddtss/words-pt_BR.txt
 * **Formato do arquivo**  : texto não formatado, UTF-8 Unicode text
 * **Ordenação dos termos**: ordem alfabética
 * **Edição das entradas** : Original em inglêsTABULAÇÃOTradução, exemplo de uso/comentário
 * **Tamanho da entrada**  : a quantidade de caracteres é indiferente

Execute o script `./exportar-vocabulario-para-ddtp-ddtss.sh` no mesmo diretório
onde está o arquivo do vocabulário (vocabulario-pt_BR.md) para fazer a
conversão automaticamente.
