a.k.a. | também conhecido como | 
ABI | Interface Binária de Aplicação (Application Binary Interface) | 
ack | notificação, reconhecimento | ver: acknowledgment
acknowledgment | notificação, reconhecimento | ver: ack
adventure | jogo de aventura | 
advisory | aviso | lit: consultoria; observar o contexto
Afrikaans | africâner | 
antispam | antispam | 
API | Interface de Programação de Aplicação (Application Programming Interface) | 
applet | miniaplicativo | 
APT pin | fixar pacote | 
APT pinning | fixar pacote | 
arcade | fliperama | 
archive | repositório, arquivamento | ocorrências comuns são "repositório Debian" ou "arquivamento compactado"
archiver | compactador, arquivador | 
artwork | ilustração | 
backbone | backbone | lit: espinha dorsal
backend | mecanismo, estrutura | 
background | de fundo, em fundo, em segundo plano | ex: "processo executando em segundo plano"
backport | backport | 
bare metal | enxuto, sem adições ou acessórios | "a bare metal server"
barre | pestana | notação musical
basename | nome do arquivo sem a extensão | 
beep | sinal sonoro, campainha, beep | 
bend | bend | notação musical
big-endian | byte mais significativo primeiro | ver: endianness
binding | vínculo | 
Birds of a Feather (BOF) | Birds of a Feather (BOF) | complemente com: reunião informal, confraternização, "desconferência"
bitrate | taxa de bits | 
blacklist | lista de bloqueio | cuidado com terminologia discriminatória
Blend | Blend | ver: Pure Blend
bloated | inchado com coisas desnecessárias | geralmente "bloated software"
bookmark | favoritos | GNOME: marcador
boot time | na inicialização, no momento ou durante a inicialização |
boot | boot, inicialização | 
boot-time | na inicialização, no momento ou durante a inicialização |
bootloader | bootloader, gerenciador de inicialização | 
bootstrap | bootstrap | 
box | máquina | lit: caixa; referência a máquinas dos portes
box | máquina | lit: caixa; referência às máquinas dos portes
Braille display | linha braille | 
Britney | Britney (script de migração para teste (testing)) | 
britney | Britney (script de migração para teste (testing)) | 
browser | navegador | 
bug tracking system | sistema de acompanhamento de bugs | 
bug-tracking-system | sistema de acompanhamento de bugs | 
bugtrackingsystem | sistema de acompanhamento de bugs | 
build | construção | se necessário, adicionar "(build)"
built in | embutido, interno | ex: comandos internos (built in) do shell
built-in | embutido, interno | ex: comandos internos (built-in) do shell
builtin | embutido, interno | ex: comandos internos (builtin) do shell
bytecode | bytecode | 
cadenza | cadenza | notação musical, não confundir com "cadência"
canonicalization | padronização, normalização | 
caret browsing | navegação por cursor | tecnologia assistiva
caret | cursor | 
case sensitive | diferencia maiúsculas e minúsculas | 
case-sensitive | diferencia maiúsculas e minúsculas | 
CD-ROM | CD-ROM | 
changelog | changelog, registro de mudanças | se for o nome do arquivo, não traduzir
character set | conjunto de caracteres | 
character | caractere | 
character-set | conjunto de caracteres | 
characterset | conjunto de caracteres | 
charset | conjunto de caracteres | forma abreviada de character set
chat | bate-papo | eventualmente manter em inglês
checksum | checksum, soma de verificação, soma de controle | 
claws mail mailer | Claws Mail | 
claws-mail-mailer | Claws Mail | 
clawsmailmailer | Claws Mail | 
CLI | interface de linha de comando | eventualmente: "linha de comando"; ver: command line interface
cloud image | imagem para computação em nuvem |
code folding | dobrar blocos de código | 
code-folding | dobrar blocos de código | 
codefolding | dobrar blocos de código | 
combo box | caixa de seleção (combo box) |
combobox | caixa de seleção (combo box) |
command line interface | interface de linha de comando | ver: CLI
community-driven distribution | distribuição orientada pela comunidade | dirigida, com decisões baseadas na comunidade
computer-aided | assistido por computador | 
concatenative speech synthesis | síntese de fala por concatenação | 
concurrency | simultaneidade | 
consistent | coerente | 
container | contêiner | 
Copy-On-Write - COW | Copiar-ao-Escrever (Copy-On-Write - COW) | 
copyright | copyright | possibilidade: direito de autor
corner case | evento improvável, mas possível |
crash dump | despejo de travamento | 
crawler | rastreador | 
cross platform | interplataforma | 
cross-platform | interplataforma | 
crossplatform | interplataforma | 
custom debian distribution | distribuição Debian personalizada | 
custom | personalizar | 
custom-debian-distribution | distribuição Debian personalizada | 
customdebiandistribution | distribuição Debian personalizada | 
daemon | daemon | 
dead | abandonado, inativo | lit.: morto
deadkey | tecla morta | 
Debian Free Software Guidelines (DFSG) | Definição Debian de Software Livre (DFSG) | 
Debian swirl | espiral do Debian |
debugging | depuração | 
decrypt | descriptografar | 
delete | excluir | 
deprecate | tornar obsoleto | 
deprecated | obsoleto | 
dereference | desreferência | 
derivative | derivado | "...distribuição derivada do Debian."
desktop environment | ambiente de área de trabalho | 
desktop | área de trabalho | 
desktop-environment | ambiente de área de trabalho | 
desktopenvironment | ambiente de área de trabalho | 
diff | diff (diferença entre arquivos), o comando diff | "há um diff (diferença entre arquivos)..."
display manager | gerenciador gráfico de login |
display | exibir | 
dock | dock, doca | 
download | baixar | 
downloader | software de download, ferramenta para download | 
drag'n'drop | arrastar e soltar | 
driver | driver | às vezes "controlador", "controladora"
drop down | suspenso | ex: menu suspenso
drop-down | suspenso | ex: menu suspenso
dropdown list | lista suspensa | 
dropdown | suspenso | ex: menu suspenso
dropdown-list | lista suspensa | 
dropdownlist | lista suspensa | 
dual boot | dual boot | 
dummy driver | driver fictício | se necessário adicionar "(dummy)"
dummy package | pacote fictício | se necessário adicionar "(dummy)"
dummy-package | pacote fictício | se necessário adicionar "(dummy)"
dummypackage | pacote fictício | se necessário adicionar "(dummy)"
dump | despejo | ver: crash dump, memory dump, etc
e.g. | por exemplo | 
embross | impressão/imprimir com relevo | tecnologia assistiva
embrosser | impressora com relevo | tecnologia assistiva, impressora térmica/fusora
encrypt | criptografar | 
endianness | ordenação de bytes (endianness) | ver: big-endian, little-endian
eqns | equações | 
experimental | experimental | "a versão experimental..."
exploit | vulnerabilidade | 
extender | repetidor | ver: repeater
eyetracker | sensor ocular | 
failover | transferência, mudança (devido a tolerância a falhas) | 
fallback | recuperação, segurança, substituto | ex: modo de recuperação, modo de segurança, pacote substituto
fallback | salvaguarda | 
FAQ | FAQ, Perguntas frequentes | 
feature | recurso | 
feed | feed | 
feedback | feedback, retroação, retroalimentação | 
fetch | busca e recuperação | de informações, pacotes, descrições...
figured bass | baixo cifrado, baixo figurado, baixo contínuo | notação musical
find | localizar | 
fingerboard | escala | notação musical
fingering | digitação | notação musical
first person shooter | tiro em primeira pessoa | 
flag | marcador (flag), sinal (flag) | lit: bandeira, sinal
for claws mail | para o Claws Mail | 
for-claws-mail | para o Claws Mail | 
forclawsmail | para o Claws Mail | 
frame | quadro, frame | "quadros (frame) de um vídeo..."
framework | infraestrutura | 
free software | software livre | 
fret | traste | notação musical
fretboard | escala | notação musical
front end | interface | 
front-end | interface | 
frontend | interface | 
full freeze | congelamento completo | sinônimo: hard freeze
gathering | coleta | 
grace period | período de tolerância | 
graphical user interface | interface gráfica do(a) usuário(a) | 
greeter | boas vindas, aplicativo de boas vindas | 
hack | hackear, alteração | às vezes: gambiarra
handbook | manual, guia, guia de referência | 
hard freeze | congelamento completo | ver: full freeze
hardcode | inserir no próprio código | codificar o programa original em questão
harden | fortalecer | 
hardware | hardware | 
headless | sem interface | 
hold | retido, suspenso | "pacotes em estado retido (hold) pelo dselect"
homepage | página web, página web de apresentação, página web inicial | 
host | máquina, servidor, host, hospedeiro | 
hotplug | hotplug | 
HOWTO | HOWTO, tutorial | "o documento HOWTO..."
HTTP request smuggling | contrabando de solicitações HTTP | ver: HTTP smuggling
HTTP smuggling | contrabando de solicitações HTTP | ver: HTTP request smuggling
i.e. | isto é, ou seja |
in a nutshell | em poucas palavras | 
incoming | repositório de entrada (incoming) | 
infrastructure | infraestrutura | 
inline tag | tag de linha única, tag embutida | 
installer | instalador, software instalador | verificar necessidade do termo "software" quando não for o Debian Installer
IRC (Internet Relay Chat) | IRC (Bate-papo de Retransmissão da Internet) | usualmente só usar a sigla
Kannada | canarês | 
kernel panic | pânico de kernel | 
kernel | kernel, kernel (núcleo) | 
keybind | associação de tecla | 
lexer | analisador léxico | 
little-endian | byte menos significativo primeiro | ver: endianness
live image | imagem de instalação "live", imagem "live" | 
logger | gravador de logs | 
logo | logotipo, logomarca | 
loopback | loopback | 
lute | alaúde | instrumento musical
magnification | ampliação | de textos, sons, desenhos; tecnologia assistiva
mailing list | lista de discussão, lista de e-mail | 
mailserver | servidor de e-mail | 
mainframe | mainframe | 
Malayalam | malaiala | 
manpage | páginas man, manpage | 
Marathi | marati | 
master | primário, principal, iniciador, requisitante | cuidado com terminologia discriminatória
measure | medida, procedimento, compasso (notação musical) | 
memory dump | despejo de memória | 
merchandise | materiais publicitários, de propaganda | também: brindes
merchandising | merchandising | indica o processo, não os produtos
middleware | middleware | 
mirroring | espelhamento | 
more information | mais informações | 
more-information | mais informações | 
moreinformation | mais informações | 
multi platform | multiplataforma | 
multi section | multissessão | 
multi-lingual | multilíngue | 
multi-platform | multiplataforma | 
multi-section | multissessão | 
multiplatform | multiplataforma | 
multisection | multissessão | 
multithread | multithread | ver: thread
namespace | espaço de nomes (namespace) |
Network Time Protocol (NTP) | Protocolo de Tempo para Redes (Network Time Protocol - NTP) |
newline | quebra de linha | pode ser a ação ou o código que faz a quebra
nonce | nonce | em criptografia, número arbitrário e de uso único
oldoldstable | oldoldstable | 
oldstable | oldstable | 
on the fly | sob demanda, em tempo real | 
on-the-fly | sob demanda, em tempo real | 
online | on-line | 
onthefly | sob demanda, em tempo real | 
open source | código aberto | 
oud | oud | instrumento musical
out of the box | pronto para uso, utilização imediata | 
out-of-the-box | pronto para uso, utilização imediata | 
overflow | estouro, sobrecarga | 
parent page | página hierarquicamente superior, página principal | 
parse | analisar, processar | 
parser | analisador, processador | 
pass-phrase | senha, frase secreta, frase-senha | 
passphrase | senha, frase secreta, frase-senha | 
patch | correção, modificação, alteração | se necessário, adicionar termo em inglês: "correção (patch)"
pentester | testadores(as) de invasão |  
performance | desempenho | 
pin | fixar pacote | ver: APT pinning
pipe | pipe, direcionamento | 
pipeline | pipeline, cadeia | 
pitch | altura | notação musical
plain text | texto não formatado, texto simples, texto puro | 
plasmoid | plasmóide | applets do KDE Plasma
platformer | jogo de plataforma | 
playlist | lista de reprodução | 
plug-in | plug-in, extensão, complemento | 
plugin | plug-in, extensão, complemento | 
point release | versão pontual | "Debian 10.1, Debian 10.2..."
pool | depósito, reservatório | "The Debian package pool..."
pop up | janela instantânea | 
pop-up | janela instantânea | 
popcon | popcon | ver: Popularity Contest
Popularity Contest | Popularity Contest (Concurso de Popularidade) | 
popup | janela instantânea | 
port | porte | 
porterbox | máquina de porte | 
porterbox | máquina de porte | 
porterboxes | máquinas de porte | 
pre seed | pré-configuração | 
pre-seed | pré-configuração | 
preseed | pré-configuração | 
pretty-printing | formatação (pretty-printing) | ajuste e realce de textos, códigos-fonte, etc.
prettyprinting | formatação (prettyprinting) | ajuste e realce de textos, códigos-fonte, etc.
prompt | prompt | 
provide | fornece, provê, disponibiliza | 
pseudopackage | pseudopacote | 
Pure Blend | Pure Blend | não traduzir, é considerado uma marca
purge | expurgar | 
race condition | condição de disputa | ex: entre dois softwares em busca de recursos de CPU
raw | não processado | 
refresh rate | taxa de atualização | 
relay | relay, retransmissão | "SMTP relay", "relay de e-mail"
release | versão, lançamento | 
rendering | renderização, processamento | 
repeater | repetidor | ver: extender
resize | redimensionar | 
rollback | reverter | 
run time | execução, executável, em tempo de execução | 
run | executar | 
run-time | execução, executável, em tempo de execução | 
runtime | execução, executável, em tempo de execução | 
sandbox | modo seguro (sandbox) | 
sane | sensato, razoável | o termo é derivado de sã/são/saudável, mas não se confundem
score | partitura | notação musical
screencast | screencast | 
screenshot | captura de tela |
scriptable | com uso de scripts, que faz uso de scripts | 
scroll | rolagem, rolagem de tela | 
scrolling game | jogo de rolagem lateral |
search | pesquisar | 
security issues | problemas de segurança | 
servlet | servlet | 
shaper | modelador de tráfego | 
shell | shell | lit.: camada, casca; geralmente referência a interfaces de linha de comando como Bash, zsh, etc.
shell | shell, camada (biologia/química) | 
shooter | jogo de tiro | 
show | mostrar | 
sibling | de mesmo nível | "pacotes de mesmo nível" (em relação à hierarquia ou função)
site | site | 
slave | secundário, subordinado, alvo, requisitado, respondente | cuidado com terminologia discriminatória
slideshow | apresentação de slides |
SLOC | linhas de código-fonte | abreviação de "source lines of code"
small caps | versaletes | Letra com a  mesma forma da maiúscula e o mesmo tamanho da minúscula.
smart host | smarthost |
smarthost | smarthost |
snapshot | snapshot | imagem/cópia criada em determinado momento; pode ser o repositório do Debian com essas imagens
sniffed | sob ataque de sniffing |
soft freeze | congelamento suave | 
software | software | 
soname | soname | 
spam | spam | 
spider | robô da lista de discussão | importante verificar o contexto
spin box | seletor numérico | 
spin-box | seletor numérico | 
spinbox | seletor numérico | 
splash screen | tela de abertura, janela de apresentação | 
sponsor | padrinho/madrinha | 
spoofer | spoofer |
stable | estável (stable); a versão estável (stable) | 
staff | pauta | notação musical; ver: stave
stand alone | autônomo, independente | 
stand-alone | autônomo, independente | 
standalone | autônomo, independente | 
standard | normalizado, padronizado |
stanza | parte de código | lit: estrofe
stave | pauta | notação musical; ver: staff
string | texto, frase, sentença, sequência de caracteres |
survival horror game | jogo de horror de sobrevivência | 
swirl | espiral | ver: Debian swirl
switcher | alternador | 
syntax highlight | realce de sintaxe | 
syntax highlighting | realce de sintaxe | 
syntax-highlight | realce de sintaxe | 
syntax-highlighting | realce de sintaxe | 
syntaxhighlight | realce de sintaxe | 
syntaxhighlighting | realce de sintaxe | 
system tray | bandeja de sistema | 
system-tray | bandeja de sistema | 
systemtray | bandeja de sistema | 
systray | bandeja de sistema | 
tarballs | arquivos tarball | 
team | time, equipe | 
template | modelo | documento de formato padronizado
testbed | banco de ensaio | 
testing | teste (testing); a versão teste (testing) | 
text folding | dobrar blocos de texto | 
text-folding | dobrar blocos de texto | 
text-to-speech (TTS) | texto-para-voz (TTS em inglês), texto-para-fala (TTS em inglês), conversão texto-fala, texto-voz | pode aparecer sem hífens
textfolding | dobrar blocos de texto | 
thread | thread, discussão | e-mails em sequência numa lista de discussão; se relacionado a subprocessos, ver: multithread
throughput | desempenho, taxa de transferência |
thumbnail | miniatura | 
thumbnailer | criador de miniaturas | 
timestamp | carimbo de tempo, marca de dia e hora | 
TODO | lista de tarefas, lista TODO, a fazer, a ser feito | 
toolchain | cadeia base de ferramentas (toolchain) | 
toolkit | kit de ferramentas | 
tooltip | dica de contexto, janela pop-up com dica | 
top-level directory | diretório mais alto na hierarquia | 
touchpad | touchpad | 
trackball | trackball | 
tracker | rastreador | 
transition freeze | congelamento de transição | 
tweak | mexer, ajustar, refinar | 
tweaker | aplicativo para ajustes | 
typeface | corpo tipográfico | 
typesetting | composição tipográfica | 
typo | erro de digitação | 
Ultimate Debian Database (UDD) | Banco de Dados Definitivo do Debian (Ultimate Debian Database - UDD) |
unattended upgrade | atualização automática | 
unhinted | não otimizadas para processamento (unhinted) | relativo à renderização de fontes
unstable | instável (unstable); a versão instável (unstable) | 
update | atualização (update) | 
upgrade | atualização (upgrade) | 
upload | upload, envio, enviado | 
upstream | autor(a) original, desenvolvedor(a) original | se necessário: "autor(a) original (upstream)"
uptime | uptime | tempo de operação com a máquina ligada
userland | espaço de usuário(a) | 
vanilla | padrão | lit.: baunilha, torna-se expressão idiomática como "vanilla software - software padrão, não customizado"
visually impaired user | usuário(a) com deficiência visual | 
web page | página web | 
web site | site web | 
web-page | página web | 
web-site | site web | 
webboot | webboot | 
webpage | página web | 
website | site web | 
Welcome team | Equipe de Boas-vindas (Welcome team) | 
whitelist | lista de permissão | cuidado com terminologia discriminatória
widget | widget | 
wiki | wiki, wiki do Debian | 
wizard | assistente | 
World Wide Web | World Wide Web | ver: WWW
WWW | WWW, World Wide Web | ver: World Wide Web
www | WWW, World Wide Web | ver: World Wide Web
WYSIWYG | WYSIWYG (What You See Is What You Get - O que você vê é o que você obtém) |
X Window System | X Window System | 
x window system | x window system | 
x-window-system | x-window-system | 
xwindowsystem | X Window System | 
union mounting | união de montagem | 
data carving | esculpimento de dados | 
file carving | esculpimento de arquivos | 
sidescroller | jogo de rolagem lateral | 
colon | dois-pontos | sinal de pontuação (:)
dungeon crawl game | jogo de exploração de masmorra | 
multilingualization | localização para vários idiomas |
unsubscribe | cancelar inscrição | 
lanyard | cordão | como usado em crachás
